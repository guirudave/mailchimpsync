<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        User::create(['name' => 'Alejandro Maya', 'email' => 'amaya@wootbit.io', 'password' => bcrypt(123456), 'mailchimp_key' => 'c2a6df6d8b0ab4b25d94abcf4d6be177-us13']);
    }
}
