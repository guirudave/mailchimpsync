<?php

use App\SyncTiempo;
use Illuminate\Database\Seeder;

class SyncTiemposTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        foreach (range(1, 24) as $hora)
            SyncTiempo::create(['hora' => date("h:i A", strtotime("$hora:00"))]);
    }
}
