<?php

use App\Pais;
use Illuminate\Database\Seeder;

class PaisesTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Pais::create(['nombre' => 'Colombia']);
        Pais::create(['nombre' => 'México']);
        Pais::create(['nombre' => 'Perú']);
        Pais::create(['nombre' => 'Ecuador']);
        Pais::create(['nombre' => 'Panamá']);
        Pais::create(['nombre' => 'Guatemala']);
        Pais::create(['nombre' => 'El Salvador']);
        Pais::create(['nombre' => 'Costa Rica']);
    }
}
