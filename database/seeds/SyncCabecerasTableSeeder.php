<?php

use App\SyncCabecera;
use Illuminate\Database\Seeder;

class SyncCabecerasTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $cabeceras = [
            ['nombre' => 'codigo', 'estatus' => 1],
            ['nombre' => 'nombre', 'estatus' => 1],
            //['nombre' => 'nombrep', 'estatus' => 0],
            //['nombre' => 'correo', 'estatus' => 1],
            //['nombre' => 'clave', 'estatus' => 0],
            ['nombre' => 'telefono', 'estatus' => 1],
            ['nombre' => 'celular', 'estatus' => 0],
            ['nombre' => 'rango', 'estatus' => 1],
            //['nombre' => 'tipo', 'estatus' => 0],
            //['nombre' => 'pais', 'estatus' => 0],
            ['nombre' => 'estado', 'estatus' => 1],
            //['nombre' => 'codigop', 'estatus' => 0],
            ['nombre' => 'creacion', 'estatus' => 1],
            ['nombre' => 'actualizacion', 'estatus' => 0]
        ];
        foreach ($cabeceras as $cabecera)
            SyncCabecera::create($cabecera);

    }
}
