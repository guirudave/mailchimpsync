<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContactosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('contactos', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('idcontrol_ci')->unique()->unsigned();
            $table->string('codigo');
            $table->string('tipo');
            $table->string('rango');
            $table->string('nombre');
            $table->string('nombrep');
            $table->string('correo');
            $table->string('telefono');
            $table->string('celular');
            $table->string('clave');
            $table->string('pais');
            $table->string('estado');
            $table->integer('codigop')->unsigned();
            $table->boolean('estatus');
            $table->boolean('b1');
            $table->boolean('b2');
            $table->boolean('b3');
            $table->boolean('b4');
            $table->boolean('b5');
            $table->boolean('b6');
            $table->boolean('b7');
            $table->boolean('b8');
            $table->boolean('b9');
            $table->timestamp('creacion');
            $table->timestamp('actualizacion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('contactos');
    }
}
