var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir.config.sourcemaps = false;
elixir(function(mix) {
    mix
        .styles([
            '../bower/angular-material/angular-material.min.css',
            '../bower/angular-material-data-table/dist/md-data-table.min.css',
            'app.css'
        ], 'public/assets/css/all.css')

        .scripts([
            '../bower/angular/angular.min.js',
            '../bower/angular-aria/angular-aria.min.js',
            '../bower/angular-animate/angular-animate.min.js',
            '../bower/angular-material/angular-material.min.js',
            '../bower/angular-resource/angular-resource.min.js',
            '../bower/angular-ui-router/release/angular-ui-router.min.js',
            '../bower/angular-sanitize/angular-sanitize.min.js',
            '../bower/angular-material-data-table/dist/md-data-table.min.js',
            '../bower/ng-file-upload/ng-file-upload.min.js',
            '../bower/lodash/dist/lodash.min.js',

            'app.js',
            'services/**/*.js',
            'directives/**/*.js',
            'controllers/**/*.js'
        ], 'public/assets/js/all.js')

        .version([
            'assets/css/all.css',
            'assets/js/all.js'
        ]);
});
