<?php

namespace App;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;

class Archivo {

    /**
     * Ruta relativa a donde se suben los archivos
     *
     * @var string
     */
    protected static $path = 'upload/archivos';

    /**
     * Obtener lista de los archivos subidos
     *
     * @return \Illuminate\Support\Collection
     */
    public static function all() {
        $data = collect();
        if (File::isDirectory(static::$path)) {
            $archivos = File::allFiles(static::$path);
            foreach ($archivos as $archivo)
                $data->push($archivo->getFileName());
        }
        return $data;
    }

    /**
     * Obetener un archivo segun el nombre proporcionado
     * En caso de no encontrar el archivo, arrojara una excepción de tipo
     * modelo no encontrado.
     *
     * @param $path
     * @return mixed
     */
    public static function findOrFail($path) {
        if (!File::exists(static::getFile($path)))
            throw (new ModelNotFoundException());
        return static::getFile($path);
    }

    /**
     * Subir achivo
     *
     * @param $file
     * @return string
     */
    public static function create($file) {
        $nombre = static::getName($file);
        $file->move(static::$path, $nombre);
        return $nombre;
    }

    /**
     * Leer archivo
     *
     * @param $path
     * @return mixed
     */
    public static function read($path) {
        $extension = pathinfo($path, PATHINFO_EXTENSION);
        $data      = ($sheets = Excel::load(static::findOrFail($path))->get()) ? ($extension !== 'csv' ? $sheets->first() : $sheets) : collect();
        return $data->transform(function ($item) {
            $data = array_change_key_case($item->toArray(), CASE_LOWER);
            array_set($data, 'correo', array_get($data, 'email', array_get($data, 'correo', '')));
            array_forget($data, 'email');
            return $data;
        });
    }

    /**
     * Unir los datos del archivo y de la lista
     *
     * @param $archivo
     * @param $lista
     * @return mixed|static
     */
    public static function merge($archivo, $lista) {
        $data1     = Archivo::read($archivo);
        $columnas1 = !$data1->isEmpty() ? array_fill_keys(array_keys($data1->first()), '') : [];

        $data2     = Lista::members($lista);
        $columnas2 = !$data2->isEmpty() ? array_fill_keys(array_keys($data2->first()), '') : [];

        $columnas = array_change_key_case(array_merge($columnas1, $columnas2), CASE_UPPER);
        foreach ([&$data1, &$data2] as &$data)
            $data->transform(function ($item) use ($columnas) {
                $data = array_merge($columnas, array_change_key_case($item, CASE_UPPER));
                ksort($data);
                return collect($data);
            });

        return collect(array_merge($data2->keyBy('CORREO')->toArray(), $data1->keyBy('CORREO')->toArray()))->values();
    }

    /**
     * Obtener ruta a archivo
     *
     * @param $path
     * @return string
     */
    private static function getFile($path) {
        return static::$path . "/$path";
    }

    /**
     * Obtener nombre del archivo para subirlo
     *
     * @param $file
     * @return string
     */
    private static function getName($file) {
        $info = collect(pathinfo($file->getClientOriginalName()));
        return str_slug(date('Y m d h i s a') . '_' . $info->get('filename'), '_') . '.' . $info->get('extension');
    }

}
