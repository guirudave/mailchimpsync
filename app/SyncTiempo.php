<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SyncTiempo extends Model {

    protected $table    = 'sync_tiempos';
    protected $fillable = ['hora', 'estatus'];
    protected $casts    = ['estatus' => 'boolean'];

    /*------------------------------------------------------------------------------
    | Scopes
    '------------------------------------------------------------------------------*/
    public function scopeActive($query) {
        return $query->where('estatus', 1);
    }

}
