<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ResponseServiceProvider extends ServiceProvider {

    public function boot() {
        response()->macro('return', function (array $params = [], $numerror = 200) {
            $args   = collect($params);
            $msg    = $args->get('msg', '');
            $data   = $args->has('model') ? $args->get('model', null) : $args->get('data', []);
            $total  = (int) $args->get('total', is_array($data) ? count($data) : empty($data));
            $errors = (array) $args->get('errors', []);
            return response()->json(compact('msg', 'data', 'total', 'errors'), $numerror);
        });
    }

    public function register() {
        //
    }

}