<?php

namespace App\Providers;

use App\Lista;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class ValidationServiceProvider extends ServiceProvider {

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        Validator::extend('current_password', function ($attribute, $value) {
            return Hash::check($value, auth()->user()->password);
        });

        Validator::extend('delete_list', function ($attribute, $value) {
            $name = Lista::find($value)->get('name', '');
            return !empty($name) && (starts_with($name, 'Asesores de Bienestar ') || starts_with($name, 'Club de Bienestar ')) ? false : true;
        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        //
    }
}