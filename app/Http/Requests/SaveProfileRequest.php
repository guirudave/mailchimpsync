<?php

namespace App\Http\Requests;

class SaveProfileRequest extends Request {

    public function rules() {
        return [
            'name'             => 'required|min:3|max:50',
            'email'            => "required|email|max:255|unique:users,email,$this->id",
            'password_current' => 'required|current_password',
            'password_new'     => 'sometimes|required|min:6|confirmed'
        ];
    }

}
