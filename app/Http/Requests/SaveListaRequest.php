<?php

namespace App\Http\Requests;

class SaveListaRequest extends Request {

    public function rules() {
        return [
            'name'                => 'required|string|min:3',
            'permission_reminder' => 'required|string|min:3'
        ];
    }
}
