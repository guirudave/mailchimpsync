<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class Request extends FormRequest {

    public function authorize() {
        return true;
    }

    public function rules() {
        return [];
    }

    public function response(array $errors) {
        return response()->return(['errors' => $errors], 422);
    }

    public function forbiddenResponse() {
        return response()->return(['msg' => trans('auth.unauthorized')], 401);
    }

}
