<?php

namespace App\Http\Requests;

class DeleteListaRequest extends Request {

    public function rules() {
        return [
            'list' => 'delete_list'
        ];
    }
}
