<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::post('login', 'SessionController@postLogin');
    Route::get('logout', 'SessionController@getLogout');

    Route::get('/', 'ViewController@app');
    Route::get('views/{view}', 'ViewController@views');
    Route::group(['middleware' => ['auth']], function () {
        Route::resource('contactos', 'ContactoController', ['only' => ['index']]);
        Route::resource('listas', 'ListaController');
        Route::resource('archivos', 'ArchivoController');
        Route::resource('sync/cabeceras', 'SyncCabeceraController', ['only' => ['index', 'update']]);
        Route::resource('sync/tiempos', 'SyncTiempoController', ['only' => ['index', 'update']]);
        Route::post('sync/{command}', 'SyncController@store')->where('command', '(lists|headers|contacts|archivo)');
        Route::resource('perfil', 'PerfilController', ['only' => ['index', 'update']]);
    });
});