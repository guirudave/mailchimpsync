<?php

namespace App\Http\Controllers;

class ViewController extends Controller {

    public function app() {
        return view('app');
    }

    public function views($view) {
        $view = "pages.$view";
        if (!view()->exists($view))
            return abort(404);
        return view($view);
    }

}