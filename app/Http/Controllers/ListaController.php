<?php

namespace App\Http\Controllers;

use App\Http\Requests\DeleteListaRequest;
use App\Http\Requests\SaveListaRequest;
use App\Lista;

class ListaController extends Controller {

    public function index() {
        $data = Lista::all()->transform(function ($item) {
            $name   = array_get($item, 'name', '');
            $locked = starts_with($name, 'Asesores de Bienestar ') || starts_with($name, 'Club de Bienestar ');
            $item   = array_add($item, 'locked', $locked);
            return $item;
        });
        return response()->return(compact('data'));
    }

    public function show($id) {
        $model = Lista::findOrFail($id);
        $model->prepend(Lista::members($id)->transform(function ($item) {
            return array_change_key_case($item, CASE_UPPER);
        }), 'members');
        return response()->return(compact('model'));
    }

    public function store(SaveListaRequest $request) {
        $data = Lista::create($request);
        return response()->return(compact('data'));
    }

    public function update(SaveListaRequest $request, $id) {
        $data = Lista::update($id, $request);
        return response()->return(compact('data'));
    }

    public function destroy(DeleteListaRequest $request, $id) {
        Lista::delete($id);
        return response()->return([]);
    }

}