<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class SessionController extends Controller {

    public function postLogin(Request $request) {
        $numerror = 422;
        $data     = ['errors' => ['email' => [trans('auth.failed')]]];
        if (auth()->attempt($request->only('email', 'password'), $request->input('remember', false))) {
            $numerror = 200;
            $data     = [];
            $user     = auth()->user();
            if (Hash::needsRehash($user->password))
                $user->update(['password' => $request->password]);
        }
        return response()->return($data, $numerror);
    }

    public function getLogout() {
        auth()->logout();
        return redirect()->to('/');
    }

}