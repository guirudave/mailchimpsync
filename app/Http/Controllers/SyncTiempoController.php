<?php

namespace App\Http\Controllers;

use App\SyncTiempo;
use Illuminate\Http\Request;

class SyncTiempoController extends Controller {

    public function index() {
        $data = SyncTiempo::all();
        return response()->return(compact('data'));
    }

    public function update($id, Request $request) {
        $model = SyncTiempo::findOrFail($id);
        $model->update($request->only('estatus'));
        return response()->return(compact('model'));
    }

}