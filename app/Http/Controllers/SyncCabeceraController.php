<?php

namespace App\Http\Controllers;

use App\SyncCabecera;
use Illuminate\Http\Request;

class SyncCabeceraController extends Controller {

    public function index() {
        $data = SyncCabecera::all();
        return response()->return(compact('data'));
    }

    public function update($id, Request $request) {
        $model = SyncCabecera::findOrFail($id);
        $model->update($request->only('estatus'));
        return response()->return(compact('model'));
    }

}