<?php

namespace App\Http\Controllers;

use App\Contacto;
use Illuminate\Http\Request;

class ContactoController extends Controller {

    public function index(Request $request) {
        $data = Contacto::with('pais')->order($request->get('order', 'nombre'))->paginate($request->get('limit', 50));
        return response()->return(compact('data'));
    }

}