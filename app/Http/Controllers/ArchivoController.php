<?php

namespace App\Http\Controllers;

use App\Archivo;
use Illuminate\Http\Request;

class ArchivoController extends Controller {

    public function index() {
        $data = Archivo::all();
        return response()->return(compact('data'));
    }

    public function show($nombre, Request $request) {
        $data = $request->has('lista') ? Archivo::merge($nombre, $request->get('lista')) : Archivo::read($nombre);
        return response()->return(compact('data'));
    }

    public function store(Request $request) {
        $model = Archivo::create($request->file('file'));
        return response()->return(compact('model'));
    }

}