<?php

namespace App\Http\Controllers;

use App\Archivo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class SyncController extends Controller {

    public function store($command, Request $request) {
        if ($command === 'archivo') {
            $listas    = ['id' => $request->get('lista', '')];
            $cabeceras = array_keys(array_filter($request->get('cabeceras', [])));
            $contactos = Archivo::read($request->get('archivo', ''));
            Artisan::call("sync:headers", compact('listas', 'cabeceras'));
            Artisan::call("sync:contacts", compact('listas', 'cabeceras', 'contactos'));
        } else {
            Artisan::call("sync:$command");
        }
        return response()->return([]);
    }

}