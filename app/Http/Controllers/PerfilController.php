<?php

namespace App\Http\Controllers;

use App\Http\Requests\SaveProfileRequest;

class PerfilController extends Controller {

    protected $user;

    public function __construct() {
        $this->user = auth()->user();
    }

    public function index() {
        $data = $this->user;
        return response()->return(compact('data'));
    }

    public function update(SaveProfileRequest $request, $id) {
        $model = $this->user;
        $model->update($request->all());
        return response()->return(compact('data'));
    }

}
