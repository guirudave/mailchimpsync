<?php

namespace App\Console;

use App\SyncTiempo;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Schema;

class Kernel extends ConsoleKernel {

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\SyncLists::class,
        Commands\SyncHeaders::class,
        Commands\SyncContacts::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule) {
        if (Schema::hasTable(with(new SyncTiempo())->getTable())) {
            $horas = SyncTiempo::active()->get();
            foreach ($horas as $hora) {
                foreach ([120 => 'lists', 60 => 'headers', 0 => 'contacts'] as $mins => $comando)
                    $schedule->command("sync:$comando")->dailyAt(date("H:i", strtotime($hora) - $mins));
            }
        }
    }
}
