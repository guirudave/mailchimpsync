<?php

namespace App\Console\Commands;

use App\Lista;
use App\SyncCabecera;
use Illuminate\Console\Command;

class SyncHeaders extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:headers {cabeceras?} {listas?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sincronizar cabeceras';

    /**
     * Inentos en obtener las listas si fueron creadas al momento de
     * intentar sincornizar las cabeceras y contactos
     *
     * @var int
     */
    protected $intentos = 0;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $operations = collect();
        $cabeceras  = empty($this->argument('cabeceras')) ? (($cabeceras = SyncCabecera::active()->get()) ? $cabeceras->pluck('nombre') : collect()) : collect($this->argument('cabeceras'));
        $listas     = empty($this->argument('listas')) ? $this->getListas() : collect([collect($this->argument('listas'))]);
        foreach ($listas as $lista)
            $operations = $operations->merge($this->syncListaCabeceras($cabeceras, $lista->get('id')));

        if (!$operations->isEmpty()) {
            Lista::batch($operations);
            sleep(15);
        }
    }

    private function getListas() {
        $this->intentos++;
        $listas = Lista::getByCountryAndType();
        if ($listas->isEmpty() && $this->intentos <= 3) {
            sleep(15);
            return $this->getListas();
        }
        return $listas;
    }

    private function syncListaCabeceras($cabeceras_app, $lista) {
        $operations   = collect();
        $cabeceras_mc = Lista::mergeFields($lista)->pluck('tag', 'merge_id');
        foreach ($cabeceras_mc->diff($cabeceras_app) as $id => $tag)
            $operations->push(['method' => 'DELETE', 'path' => "lists/$lista/merge-fields/$id"]);

        foreach ($cabeceras_app->diff($cabeceras_mc) as $id => $tag)
            $operations->push(['method' => 'POST', 'path' => "lists/$lista/merge-fields", 'body' => collect(['tag' => $tag, 'name' => ucfirst(strtolower($tag)), 'type' => 'text'])->toJson()]);

        return $operations;
    }

}
