<?php

namespace App\Console\Commands;

use App\Lista;
use App\Pais;
use Illuminate\Console\Command;

class SyncLists extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:lists';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sincronizar listas';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $espera = false;
        $paises = Pais::all();
        $tipos  = Lista::types();
        $listas = Lista::all();
        foreach ($paises as $pais)
            foreach ($tipos as $tipo) {
                $nombre = $tipo->get('nombre') . ' ' . $pais->nombre;
                if (!$listas->whereLoose('name', $nombre)->first()) {
                    Lista::create(collect(['name' => $nombre, 'permission_reminder' => $nombre]));
                    $espera = true;
                }
            }
        if ($espera)
            sleep(15);
    }

}