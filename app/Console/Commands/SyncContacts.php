<?php

namespace App\Console\Commands;

use App\Contacto;
use App\Lista;
use App\SyncCabecera;
use Illuminate\Console\Command;

class SyncContacts extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:contacts {listas?} {cabeceras?} {contactos?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sincronizar contactos';

    /**
     * Inentos en obtener las listas si fueron creadas al momento de
     * intentar sincornizar las cabeceras y contactos
     *
     * @var int
     */
    protected $intentos = 0;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $operations = collect();
        $contactos  = empty($this->argument('contactos')) ? Contacto::where('estatus', 1)->where('b1', 1)->get() : collect($this->argument('contactos'));
        $cabeceras  = empty($this->argument('cabeceras')) ? (($cabeceras = SyncCabecera::active()->get()) ? $cabeceras->pluck('nombre') : collect()) : collect($this->argument('cabeceras'));
        $listas     = empty($this->argument('listas')) ? $this->getListas() : collect([collect($this->argument('listas'))]);

        foreach ($listas as $lista) {
            $miembros   = ($lista->has('pais') && $lista->has('tipo')) ? $contactos->whereLoose('pais', $lista->get('pais'))->whereLoose('tipo', $lista->get('tipo'))->keyBy('correo') : $contactos;
            $miembros   = $miembros->filter(function ($value) {
                return array_get($value, 'correo', '') !== '';
            })->keyBy('correo');
            $operations = $operations->merge($this->syncMiembros($lista->get('id'), $miembros, $cabeceras));
        }

        if (!$operations->isEmpty())
            Lista::batch($operations);
    }

    private function getListas() {
        $this->intentos++;
        $listas = Lista::getByCountryAndType();
        if ($listas->isEmpty() && $this->intentos <= 3) {
            sleep(15);
            return $this->getListas();
        }
        return $listas;
    }

    private function syncMiembros($lista, $miembros, $cabeceras) {
        $operations   = collect();
        $miembros_app = $miembros->pluck('correo');
        $miembros_mc  = Lista::members($lista)->pluck('correo');

        /*foreach (['subscribed' => $miembros_mc->intersect($miembros_app), 'unsubscribed' => $miembros_mc->diff($miembros_app)] as $status => $correos)
            foreach ($correos as $correo)
                $operations->push(['method' => 'PATCH', 'path' => "lists/$lista/members/" . md5($correo), 'body' => collect(['email_address' => $correo, 'status' => $status, 'merge_fields' => $this->syncMiembroCabeceras($cabeceras, $miembros->get($correo, []))])->toJson()]);*/

        foreach ($miembros_mc->diff($miembros_app) as $correo)
            $operations->push(['method' => 'DELETE', 'path' => "lists/$lista/members/" . md5($correo)]);

        foreach ($miembros_mc->intersect($miembros_app) as $correo)
            $operations->push(['method' => 'PATCH', 'path' => "lists/$lista/members/" . md5($correo), 'body' => collect(['email_address' => $correo, 'status' => 'subscribed', 'merge_fields' => $this->syncMiembroCabeceras($cabeceras, $miembros->get($correo, []))])->toJson()]);

        foreach ($miembros_app->diff($miembros_mc) as $correo)
            $operations->push(['method' => 'POST', 'path' => "lists/$lista/members", 'body' => collect(['email_address' => $correo, 'status' => 'subscribed', 'merge_fields' => $this->syncMiembroCabeceras($cabeceras, $miembros->get($correo, []))])->toJson()]);

        return $operations;
    }

    private function syncMiembroCabeceras($cabeceras_app, $miembro) {
        $miembro      = !is_array($miembro) ? $miembro->toArray() : $miembro;
        $cabeceras_mc = [];
        foreach ($cabeceras_app as $cabecera)
            if (isset($miembro[strtolower($cabecera)]))
                $cabeceras_mc[$cabecera] = $miembro[strtolower($cabecera)];
        return $cabeceras_mc;
    }

}
