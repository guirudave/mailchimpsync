<?php

namespace App;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Mailchimp\Mailchimp;

class Lista {

    private $malchimp;

    public function __construct() {
        $apikey         = auth()->user()->mailchimp_key;
        $this->malchimp = new Mailchimp($apikey);
    }

    /**
     * Obtener objeto Mailchimp
     *
     * @return mixed
     */
    private static function mc() {
        return with(new Lista())->malchimp;
    }

    /**
     * Obtener todas las litas desde mailchimp
     *
     * @return \Illuminate\Support\Collection
     */
    public static function all() {
        try {
            return static::convertToCollection(static::mc()->get("lists", ['offset' => 0, 'count' => 1000])->get('lists', []));
        } catch (\Exception $e) {
            return collect();
        }
    }

    /**
     * Obtener una lista desde mailchimp según el id proporcionado
     *
     * @param $id
     * @return \Illuminate\Support\Collection
     */
    public static function find($id) {
        try {
            return static::convertToCollection(static::mc()->get("lists/$id")->toArray());
        } catch (\Exception $e) {
            return collect();
        }
    }

    /**
     * Obtener una lista desde mailchimp según el id proporcionado
     * En caso de no encontrar la lista, arrojara una excepción de tipo
     * modelo no encontrado.
     *
     * @param $id
     * @return \Illuminate\Support\Collection
     */
    public static function findOrFail($id) {
        try {
            return static::convertToCollection(static::mc()->get("lists/$id")->toArray());
        } catch (\Exception $e) {
            throw (new ModelNotFoundException());
        }
    }

    /**
     * Crear una lista en mailchimp
     *
     * @param $data
     * @return \Illuminate\Support\Collection
     */
    public static function create($data) {
        try {
            return static::convertToCollection(static::mc()->post('lists', static::getListData($data))->toArray());
        } catch (\Exception $e) {
            throw (new ModelNotFoundException());
        }
    }

    /**
     * Actualizar una lista en mailchimp
     *
     * @param $id
     * @param $data
     * @return \Illuminate\Support\Collection
     */
    public static function update($id, $data) {
        try {
            return static::convertToCollection(static::mc()->patch("lists/$id", static::getListData($data))->toArray());
        } catch (\Exception $e) {
            throw (new ModelNotFoundException());
        }
    }

    /**
     * Eliminar una lista en mailchimp
     *
     * @param $id
     * @return mixed
     */
    public static function delete($id) {
        try {
            return static::mc()->delete("lists/$id");
        } catch (\Exception $e) {
            throw (new ModelNotFoundException());
        }
    }

    /**
     * Enviar un listado de operaciones para ejecutar en mailchimp
     *
     * @param $operations
     */
    public static function batch($operations) {
        try {
            static::mc()->post('batches', compact('operations'));
        } catch (\Exception $e) {
            throw (new ModelNotFoundException());
        }
    }

    /**
     * Obtener una lista de las listas desde mailchimp
     * clasificadas por pais y tipo
     *
     * @return \Illuminate\Support\Collection
     */
    public static function getByCountryAndType() {
        $data   = collect();
        $paises = Pais::all();
        $tipos  = static::types();
        $listas = static::all();
        foreach ($paises as $pais)
            foreach ($tipos as $tipo) {
                $nombre = $tipo->get('nombre') . ' ' . $pais->nombre;
                $lista  = collect($listas->whereLoose('name', $nombre)->first());
                if (!$lista->isEmpty())
                    $data->push(collect(['pais' => $pais->id, 'tipo' => $tipo->get('id'), 'id' => $lista->get('id')]));
            }
        return $data;
    }

    /**
     * Lista estática de tipos de contactos
     *
     * @return \Illuminate\Support\Collection
     */
    public static function types() {
        return collect([
            collect(['id' => 'CI', 'nombre' => 'Asesores de Bienestar']),
            collect(['id' => 'CLUB', 'nombre' => 'Club de Bienestar'])
        ]);
    }

    /**
     * Obtener los miembros de una lista
     *
     * @param $id
     * @return \Illuminate\Support\Collection|static
     */
    public static function members($id) {
        try {
            return static::convertToCollection(static::mc()->get("lists/$id/members", ['offset' => 0, 'count' => 1000])->get('members', []))->whereLoose('status', 'subscribed')->transform(function ($item) {
                return array_change_key_case(array_merge(['correo' => $item['email_address']], $item['merge_fields']), CASE_LOWER);
            })->values();
        } catch (\Exception $e) {
            return collect();
        }
    }

    /**
     * Obtener los campos adicionales de la lista
     *
     * @param $id
     * @return \Illuminate\Support\Collection
     */
    public static function mergeFields($id) {
        try {
            return static::convertToCollection(static::mc()->get("lists/$id/merge-fields", ['offset' => 0, 'count' => 1000])->get('merge_fields', []));
        } catch (\Exception $e) {
            return collect();
        }
    }

    /**
     * Obtener un arreglo de los datos necesarios para crear la lista
     *
     * @param $data
     * @return array
     */
    private static function getListData($data) {
        return [
            'name'                => $data->get('name'),
            'permission_reminder' => $data->get('permission_reminder'),
            'email_type_option'   => false,
            'campaign_defaults'   => [
                'from_name'  => env('LIST_FROM_NAME', 'Name'),
                'from_email' => env('LIST_FROM_EMAIL', 'email@test.com'),
                'subject'    => env('LIST_SUBJECT', 'Subject'),
                'language'   => env('LIST_LANGUAGE', 'ES')
            ],
            'contact'             => [
                'company'  => env('LIST_COMPANY', 'Company'),
                'address1' => env('LIST_ADDRESS1', 'Address'),
                'address2' => env('LIST_', ''),
                'city'     => env('LIST_CITY', 'City'),
                'state'    => env('LIST_STATE', 'State'),
                'zip'      => env('LIST_ZIP', '1234'),
                'country'  => env('LIST_COUNTRY', 'MX')
            ]
        ];
    }

    /**
     * Convertir las respuestas de la API del mailchimp
     * en collecciones
     *
     * @param $data
     * @return \Illuminate\Support\Collection
     */
    private static function convertToCollection($data) {
        return collect(json_decode(json_encode($data), true));
    }

}
