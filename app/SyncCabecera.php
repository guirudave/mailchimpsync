<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SyncCabecera extends Model {

    protected $table    = 'sync_cabeceras';
    protected $fillable = ['nombre', 'estatus'];
    protected $casts    = ['estatus' => 'boolean'];

    /*------------------------------------------------------------------------------
    | Scopes
    '------------------------------------------------------------------------------*/
    public function scopeActive($query) {
        return $query->where('estatus', 1);
    }

    /*------------------------------------------------------------------------------
    | Accesors & Mutators
    '------------------------------------------------------------------------------*/
    public function getNombreAttribute($value) {
        return strtoupper($value);
    }

}
