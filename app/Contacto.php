<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contacto extends Model {

    public    $timestamps = false;
    protected $connection = 'externa';
    protected $table      = 'control_ci';
    protected $casts      = ['estatus' => 'boolean', 'b1' => 'boolean', 'b2' => 'boolean', 'b3' => 'boolean', 'b4' => 'boolean', 'b5' => 'boolean', 'b6' => 'boolean', 'b7' => 'boolean', 'b8' => 'boolean', 'b9' => 'boolean'];

    /*------------------------------------------------------------------------------
    | Relations
    '------------------------------------------------------------------------------*/
    public function pais() {
        return $this->belongsTo(Pais::class, 'pais');
    }

    /*------------------------------------------------------------------------------
    | Scopes
    '------------------------------------------------------------------------------*/
    public function scopeOrder($query, $value) {
        $field = str_replace('-', '', $value);
        $dir   = starts_with($value, '-') ? 'desc' : 'asc';
        return $query->orderBy($field, $dir);
    }

}
