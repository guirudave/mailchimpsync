<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Crypt;

class User extends Authenticatable {

    protected $fillable = ['name', 'email', 'password', 'mailchimp_key'];
    protected $hidden   = ['password', 'remember_token'];

    /*------------------------------------------------------------------------------
    | Accessors && Mutators
    '------------------------------------------------------------------------------*/
    public function setMailchimpKeyAttribute($value) {
        $this->attributes['mailchimp_key'] = Crypt::encrypt($value);
    }

    public function getMailchimpKeyAttribute($value) {
        return !empty($value) ? Crypt::decrypt($value) : '';
    }

}
