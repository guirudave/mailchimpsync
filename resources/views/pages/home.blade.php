<div layout="column" flex>
    {{-- Topbar --}}
    <topbar></topbar>
    {{-- Content --}}
    <section layout="row" layout-fill flex>
        {{-- Leftbar --}}
        <leftbar></leftbar>
        {{-- Container --}}
        <div class="container" layout="row" layout-padding flex ui-view></div>
    </section>
</div>