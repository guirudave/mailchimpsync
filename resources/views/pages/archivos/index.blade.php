<md-card layou="column" flex ng-init="vm.getData()">

    {{-- Titulo --}}
    <md-toolbar class="md-table-toolbar md-default">
        <div class="md-toolbar-tools">
            <h2 class="md-title" flex>Archivos</h2>
        </div>
    </md-toolbar>

    {{-- Lista de archivos --}}
    <md-toolbar class="md-table-toolbar md-default">
        <div class="md-toolbar-tools">
            <md-input-container flex>
                <md-select ng-model="vm.archivo" aria-label="archivos" ng-change="vm.select()" ng-disabled="vm.isLoading" style="padding-right: 20px;">
                    <md-option ng-repeat="archivo in vm.archivos" value="@{{ archivo }}">@{{ archivo }}</md-option>
                </md-select>
            </md-input-container>
            <md-button class="md-default md-raised md-icon-button" type="file" accept=".xls,.csv,.xlsx" ngf-max-size="2MB" ngf-select="vm.upload()" ng-model="vm.file" ng-disabled="vm.isLoading">
                <md-tooltip md-direction="left">Subir archivo</md-tooltip>
                <md-icon>file_upload</md-icon>
            </md-button>
        </div>
    </md-toolbar>

    {{-- Lista de listas --}}
    <md-toolbar class="md-table-toolbar md-default">
        <div class="md-toolbar-tools">
            <md-input-container flex>
                <md-select ng-model="vm.lista" aria-label="listas" ng-change="vm.select()" ng-disabled="vm.isLoading" style="padding-right: 20px;">
                    <md-option ng-repeat="lista in vm.listas | filter:{locked: false}" value="@{{ lista.id }}">@{{ lista.name }}</md-option>
                </md-select>
            </md-input-container>
            <md-button class="md-default md-raised md-icon-button" ng-click="vm.merge()" ng-disabled="vm.isLoading || (!vm.archivo || !vm.lista || !vm.columnas)">
                <md-tooltip md-direction="left">Unir contactos</md-tooltip>
                <md-icon>compare_arrows</md-icon>
            </md-button>
        </div>
        <md-divider></md-divider>
        <md-progress-linear md-mode="indeterminate" ng-show="vm.isLoading" flex></md-progress-linear>
    </md-toolbar>

    {{-- Tabla de datos del archivo y la lista --}}
    <md-table-container flex>
        <table md-table md-progress="vm.promise">
            <thead md-head>
                <tr md-row>
                    <th md-column ng-repeat="(key, value) in vm.contactos[0]">
                        <md-checkbox class="md-secondary" aria-label="columna_@{{ key }}" ng-model="vm.columnas[key]" ng-disabled="vm.isLoading || key == 'CORREO'"></md-checkbox>
                        @{{ key }}
                    </th>
                </tr>
            </thead>
            <tbody md-body>
                <tr md-row ng-repeat="contacto in vm.contactos">
                    <td md-cell ng-repeat="(key, value) in contacto">@{{ value }}</td>
                </tr>
            </tbody>
        </table>
    </md-table-container>

</md-card>