<md-card layou="column" flex ng-init="vm.getData()">

    <md-toolbar class="md-table-toolbar md-default">
        <div class="md-toolbar-tools">
            <h2 class="md-title" flex>Sincronización</h2>
            <md-button class="md-default md-raised md-icon-button" ng-click="vm.sync()" ng-disabled="vm.isLoading">
                <md-icon ng-class="{'md-spin': vm.syncing}">sync</md-icon>
                <md-tooltip md-direction="left">Sincronizar</md-tooltip>
            </md-button>
        </div>
        <md-divider></md-divider>
        <md-progress-linear md-mode="indeterminate" ng-show="vm.isLoading" flex></md-progress-linear>
    </md-toolbar>

    <md-content layout-gt-xs="row" layout-margin flex>
        <md-list flex>
            <md-subheader class="md-primary md-no-sticky">Horas</md-subheader>
            <md-list-item ng-repeat="tiempo in vm.tiempos">
                <p>@{{ tiempo.hora }}</p>
                <md-checkbox class="md-secondary" ng-model="tiempo.estatus" ng-change="vm.check('tiempos', tiempo)" ng-disabled="vm.isLoading"></md-checkbox>
            </md-list-item>
        </md-list>

        <md-divider></md-divider>

        <md-list flex>
            <md-subheader class="md-primary md-no-sticky">Cabeceras</md-subheader>
            <md-list-item ng-repeat="cabecera in vm.cabeceras">
                <p>@{{ cabecera.nombre }}</p>
                <md-checkbox class="md-secondary" ng-model="cabecera.estatus" ng-change="vm.check('cabeceras', cabecera)" ng-disabled="vm.isLoading"></md-checkbox>
            </md-list-item>
        </md-list>
    </md-content>

</md-card>