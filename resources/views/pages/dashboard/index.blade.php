<md-content class="container" layout="row" layout-padding layout-margin layout-fill layout-wrap>
    <md-card layout="column" ng-repeat="item in [1, 2, 3, 4, 5]" style="width: 350px; max-height: 200px;">
        <md-card-content flex>
            <h2>Titulo</h2>
            <p>Descripción</p>
        </md-card-content>
        <md-actions layout="row" layout-align="end center">
            <md-button class="md-raised">
                <md-icon>search</md-icon>
                Ver
            </md-button>
        </md-actions>
    </md-card>
</md-content>