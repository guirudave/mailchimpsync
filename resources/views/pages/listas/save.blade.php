@extends('layouts.save-dialog')

@section('title')
    @{{ vm.isEdit ? 'Editar' : 'Crear' }} Lista
@endsection

@section('fields')
    {{--<h3 class="md-subhead">Descripción</h3>--}}
    <md-input-container class="md-block">
        <label>Nombre</label>
        <input type="text" name="name" ng-model="vm.model.name" required ng-readonly="vm.isEdit"/>
    </md-input-container>
    <md-input-container class="md-block">
        <label>Recordar a la gente la forma en que se inscribieron a la lista</label>
        <textarea name="permission_reminder" ng-model="vm.model.permission_reminder" required ng-readonly="vm.isEdit"></textarea>
    </md-input-container>
    {{--<md-input-container class="md-block">
        <label>Nombre del Remitente</label>
        <input name="from_name" ng-model="vm.model.from_name" required/>
    </md-input-container>
    <md-input-container class="md-block">
        <label>Email del Remitente</label>
        <input type="email" name="from_email" ng-model="vm.model.from_email" required/>
    </md-input-container>

    <h3 class="md-subhead">Contact</h3>
    <md-input-container class="md-block">
        <label>Compañía/Organización</label>
        <input name="company" ng-model="vm.model.company" required/>
    </md-input-container>
    <md-input-container class="md-block">
        <label>Dirección 1</label>
        <input name="address1" ng-model="vm.model.address1" required/>
    </md-input-container>
    <md-input-container class="md-block">
        <label>Dirección 2</label>
        <input name="address2" ng-model="vm.model.address2" required/>
    </md-input-container>
    <md-input-container class="md-block">
        <label>Teléfono</label>
        <input name="phone" ng-model="vm.model.phone" required/>
    </md-input-container>
    <md-input-container class="md-block">
        <label>Ciudad</label>
        <input name="city" ng-model="vm.model.city" required/>
    </md-input-container>
    <md-input-container class="md-block">
        <label>Estado</label>
        <input name="state" ng-model="vm.model.state" required/>
    </md-input-container>
    <md-input-container class="md-block">
        <label>País</label>
        <input name="country" ng-model="vm.model.country" required/>
    </md-input-container>
    <md-input-container class="md-block">
        <label>Zip</label>
        <input name="zip" ng-model="vm.model.zip" required/>
    </md-input-container>--}}
@endsection