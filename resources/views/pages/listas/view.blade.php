<md-card layou="column" flex ng-init="vm.getData()">

    <md-toolbar class="md-table-toolbar md-default">
        <div class="md-toolbar-tools" flex>
            <h2 class="md-title" flex>
                <span ng-show="vm.lista.name">Lista "@{{ vm.lista.name }}"</span>
                <span ng-show="!vm.lista.name">Cargando lista</span>
            </h2>
            <md-button class="md-default md-raised md-icon-button" ui-sref="home.listas">
                <md-tooltip md-direction="bottom">Ir a Listas</md-tooltip>
                <md-icon>navigate_before</md-icon>
            </md-button>
        </div>
        <md-divider></md-divider>
        <md-progress-linear md-mode="indeterminate" ng-show="vm.isLoading" flex></md-progress-linear>
    </md-toolbar>

    <md-table-container flex>
        <table md-table md-progress="vm.promise">
            <thead md-head>
                <tr md-row>
                    <th md-column ng-repeat="(key, value) in vm.lista.members[0]">@{{ key }}</th>
                </tr>
            </thead>
            <tbody md-body>
                <tr md-row ng-repeat="miembro in vm.lista.members">
                    <td md-cell ng-repeat="(key, value) in miembro">@{{ value }}</td>
                </tr>
            </tbody>
        </table>
    </md-table-container>

</md-card>