<md-card layou="column" flex ng-init="vm.getData()">

    {{-- Titulo --}}
    <md-toolbar class="md-table-toolbar md-default">
        <div class="md-toolbar-tools">
            <h2 class="md-title" flex>Listas</h2>
            <md-button class="md-default md-raised md-icon-button" ng-click="vm.create($event)">
                <md-tooltip md-direction="left">Crear Lista</md-tooltip>
                <md-icon>add</md-icon>
            </md-button>
        </div>
        <md-divider></md-divider>
    </md-toolbar>

    {{-- Tabla de datos --}}
    <md-table-container flex>
        <table md-table md-progress="vm.promise">
            <thead md-head>
                <tr md-row>
                    <th md-column class="text-center" style="width: 100px;">CÓDIGO</th>
                    <th md-column>NOMBRE</th>
                    <th md-column class="text-center" style="width: 75px;">MIEMBROS</th>
                    <th md-column style="width: 100px;"></th>
                </tr>
            </thead>
            <tbody md-body>
                <tr md-row ng-show="!vm.listas.length">
                    <td md-cell colspan="4">Sin registros</td>
                </tr>
                <tr md-row ng-repeat="lista in vm.listas">
                    <td md-cell class="text-center">@{{ lista.id }}</td>
                    <td md-cell>@{{ lista.name }}</td>
                    <td md-cell class="text-center">@{{ lista.stats.member_count }}</td>
                    <td md-cell layout="row" layout-align="center center" style="padding-right: 0;">
                        <md-button class="md-icon-button md-default" ui-sref="home.listas-view({id: lista.id})">
                            <md-tooltip md-direction="bottom">Ver</md-tooltip>
                            <md-icon>search</md-icon>
                        </md-button>
                        <md-button class="md-icon-button md-accent" ng-click="vm.edit($event, lista)">
                            <md-tooltip md-direction="bottom">Editar</md-tooltip>
                            <md-icon>mode_edit</md-icon>
                        </md-button>
                        <md-button class="md-icon-button md-warn" ng-click="vm.delete($event, lista)" ng-disabled="lista.locked">
                            <md-tooltip md-direction="bottom">Eliminar</md-tooltip>
                            <md-icon>delete</md-icon>
                        </md-button>
                    </td>
                </tr>
            </tbody>
        </table>
    </md-table-container>

</md-card>