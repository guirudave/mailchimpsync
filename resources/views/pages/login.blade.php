<md-content layout="row" layout-align="center center" class="container" flex>
    <md-card style="width: 300px;">
        <md-progress-linear md-mode="indeterminate" ng-show="vm.isLoading" flex></md-progress-linear>
        <md-card-header>
            <md-card-header-text>
                <span class="md-title">Inicio de Sesión</span>
            </md-card-header-text>
        </md-card-header>
        <md-card-content flex>
            <form name="vm.form" ng-submit="vm.login(vm.model)">
                <md-input-container class="md-block">
                    <label>Correo</label>
                    <input name="email" type="email" ng-model="vm.model.email" required/>
                </md-input-container>
                <md-input-container class="md-block">
                    <label>Contraseña</label>
                    <input name="password" type="password" ng-model="vm.model.password" required/>
                </md-input-container>
                <md-input-container class="md-block">
                    <md-checkbox ng-model="vm.model.remember" aria-label="Recordarme">Recordarme</md-checkbox>
                    <md-button class="md-primary" ng-click="vm.login(vm.model)" ng-disabled="vm.form.$invalid || vm.isLoading">Entrar</md-button>
                </md-input-container>
                @include('includes.errors-msg-server')
            </form>
        </md-card-content>
    </md-card>
</md-content>