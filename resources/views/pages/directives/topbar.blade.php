<md-toolbar layout="row" layout-align="start center">

    <md-button class="md-icon-button" ng-click="vm.toggleLeftbar()" hide-gt-sm>
        <md-icon>menu</md-icon>
    </md-button>

    <h1 class="md-subhead toolbar-subhead" flex>MAILCHIMPSYNC</h1>

    <md-menu md-position-mode="target-right target">
        <md-button class="toolbar-button" ng-click="$mdOpenMenu($event)">
            <md-icon>person</md-icon>
            <span hide-xs>{{ auth()->check() ? auth()->user()->name : 'Usuario' }}</span>
        </md-button>
        <md-menu-content width="4">
            <md-menu-item>
                <md-button ng-click="vm.profile($event)">
                    <md-icon>face</md-icon>
                    <p>Perfil</p>
                </md-button>
            </md-menu-item>
            <md-menu-item>
                <md-button ng-click="vm.logout()">
                    <md-icon>exit_to_app</md-icon>
                    <p>Salir</p>
                </md-button>
            </md-menu-item>
        </md-menu-content>
    </md-menu>

</md-toolbar>