<md-sidenav class="md-sidenav-left md-whiteframe-z2" md-component-id="leftbar" md-is-locked-open="$mdMedia('gt-sm')" style="max-width: 250px;">
    <md-list>
        <md-list-item ui-sref="home.dashboard" ui-sref-active="selected">
            <md-icon>dashboard</md-icon>
            <p>Dashboard</p>
        </md-list-item>

        <md-list-item ui-sref="home.contactos" ui-sref-active="selected">
            <md-icon>contacts</md-icon>
            <p>Contactos</p>
        </md-list-item>

        <md-list-item ui-sref="home.listas" ui-sref-active="selected">
            <md-icon>list</md-icon>
            <p>Listas</p>
        </md-list-item>

        <md-list-item ui-sref="home.archivos" ui-sref-active="selected">
            <md-icon>insert_drive_file</md-icon>
            <p>Archivos</p>
        </md-list-item>

        <md-list-item ui-sref="home.sincronizacion" ui-sref-active="selected">
            <md-icon>sync</md-icon>
            <p>Sincronización</p>
        </md-list-item>
    </md-list>
</md-sidenav>