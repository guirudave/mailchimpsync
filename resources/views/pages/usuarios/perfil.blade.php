@extends('layouts.save-dialog')

@section('title')
    Perfil
@endsection

@section('fields')
    <md-input-container class="md-block">
        <label>Nombre</label>
        <input type="text" name="name" ng-model="vm.model.name" required/>
    </md-input-container>
    <md-input-container class="md-block">
        <label>Correo</label>
        <input type="email" name="email" ng-model="vm.model.email" required/>
    </md-input-container>
    <md-input-container class="md-block">
        <label>Mailchimp API Key</label>
        <input type="text" name="mailchimp_key" ng-model="vm.model.mailchimp_key" required/>
    </md-input-container>
    <md-input-container class="md-block">
        <label>Contraseña actual</label>
        <input type="password" name="password_current" ng-model="vm.model.password_current" required/>
    </md-input-container>
    <md-input-container class="md-block">
        <label>Contraseña nueva</label>
        <input type="password" name="password_new" ng-model="vm.model.password_new" ng-disabled="!vm.model.password_current"/>
    </md-input-container>
    <md-input-container class="md-block">
        <label>Confirmar contraseña</label>
        <input type="password" name="password_new_confirmation" ng-model="vm.model.password_new_confirmation" ng-disabled="!vm.model.password_current"/>
    </md-input-container>
@endsection