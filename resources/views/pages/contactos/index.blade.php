<md-card layou="column" flex ng-init="vm.getData()">

    <md-toolbar class="md-table-toolbar md-default">
        <div class="md-toolbar-tools">
            <h2 class="md-title" flex>Contacts</h2>
        </div>
        <md-divider></md-divider>
    </md-toolbar>

    <md-table-container flex>
        <table md-table md-progress="vm.promise">
            <thead md-head md-order="vm.params.order" md-on-reorder="vm.getData">
                <tr md-row>
                    <th md-column md-order-by="codigo" class="text-center" style="width: 100px;">CÓDIGO</th>
                    <th md-column md-order-by="nombre" style="min-width: 200px;">NOMBRE</th>
                    <th md-column class="text-center">RANGO</th>
                    <th md-column class="text-center">PAÍS</th>
                    <th md-column>CREACIÓN</th>
                    <th md-column>MODIFIACIÓN</th>
                    <th md-column class="text-center" style="width: 50px;">ESTATUS</th>
                </tr>
            </thead>
            <tbody md-body>
                <tr md-row ng-show="!vm.contactos.data.length">
                    <td md-cell colspan="7">Sin registros</td>
                </tr>
                <tr md-row ng-repeat="contacto in vm.contactos.data">
                    <td md-cell class="text-center">@{{ contacto.codigo }}</td>
                    <td md-cell>@{{ contacto.nombre }}</td>
                    <td md-cell class="text-center">@{{ contacto.rango }}</td>
                    <td md-cell class="text-center">@{{ contacto.pais.nombre }}</td>
                    <td md-cell>@{{ contacto.creacion }}</td>
                    <td md-cell>@{{ contacto.actualizacion }}</td>
                    <td md-cell class="text-center">
                        <md-icon>@{{ contacto.estatus && contacto.b1 ? 'check_box' : 'check_box_outline_blank' }}</md-icon>
                    </td>
                </tr>
            </tbody>
        </table>
    </md-table-container>

    <md-card-footer style="padding: 0;">
        <md-table-pagination
                md-page-select
                md-page="vm.params.page"
                md-on-paginate="vm.getData"
                md-limit-options="[10, 25, 50, 100, 250]"
                md-limit="vm.params.limit"
                md-total="@{{ vm.contactos.total }}"
                md-label="@{{ vm.label }}"
                md-boundary-links="true">
        </md-table-pagination>
    </md-card-footer>

</md-card>