<!doctype html>
<html ng-app="App">
    <head>
        <title>MailchimpSync</title>
        <base href="{{ url('/') }}/">
        <meta charset="utf-8">
        <meta name="author" content="">
        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,400italic" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="{{ asset(elixir('assets/css/all.css')) }}" rel="stylesheet">
    </head>
    <body ng-app="App" ng-cloak>
        <!--[if lt IE 10]>
        <p style="padding: 20px;">
            You are using an <strong>outdated</strong> browser.
            Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.
        </p>
        <![endif]-->
        <div layout="row" layout-fill ui-view></div>
        <script src="{{ asset(elixir('assets/js/all.js')) }}"></script>
        <script type="text/javascript">{{ 'var _session = '.(auth()->check() ? 'true' : 'false').';' }}</script>
    </body>
</html>