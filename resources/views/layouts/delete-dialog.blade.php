<md-dialog flex-xs="90" flex-gt-xs="75" flex-gt-sm="60" flex-gt-md="45">
    <md-progress-linear md-mode="indeterminate" ng-show="vm.isLoading" flex></md-progress-linear>
    <md-dialog-content class="md-dialog-content">
        <h2 class="md-title">Confirmación</h2>
        <p>Está seguro que quiere eliminar @yield('reference')?</p>
        @include('includes.errors-msg-server')
    </md-dialog-content>
    <md-dialog-actions>
        <md-button class="md-default" ng-click="vm.cancel()">Cancelar</md-button>
        <md-button class="md-warn" ng-click="vm.delete()">Eliminar</md-button>
    </md-dialog-actions>
</md-dialog>