<md-dialog flex-xs="90" flex-gt-xs="75" flex-gt-sm="60" flex-gt-md="45" style="max-height: 95%;">
    <md-progress-linear md-mode="indeterminate" ng-show="vm.isLoading" flex></md-progress-linear>
    <md-dialog-content class="md-dialog-content">
        <h2 class="md-title">@yield('title')</h2>
        <form name="vm.form" ng-submit="vm.save(vm.model)">@yield('fields')</form>
        @include('includes.errors-msg-server')
    </md-dialog-content>
    <md-dialog-actions>
        <md-button class="md-default" ng-click="vm.cancel()">Cancelar</md-button>
        <md-button class="md-primary" ng-click="vm.save(vm.model)" ng-disabled="vm.form.$invalid || vm.isLoading">Guardar</md-button>
    </md-dialog-actions>
</md-dialog>