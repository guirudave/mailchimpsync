<div ng-repeat="error in vm.errors" layout="row">
    <md-icon class="md-warn">error_outline</md-icon>
    <md-subheader class="md-warn" flex>@{{ error[0] }}</md-subheader>
</div>