(function() {
    'use strict';
    angular.module('App').directive('leftbar', function() {
        return {
            templateUrl: 'views/directives.leftbar',
            restrict: 'E',
            replace: true
        }
    });
}());