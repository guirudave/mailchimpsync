(function() {
    'use strict';
    angular.module('App').directive('topbar', function() {
        return {
            templateUrl: 'views/directives.topbar',
            restrict: 'E',
            replace: true
        }
    });
}());