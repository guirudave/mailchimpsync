(function() {
    'use strict';
    angular.module('App').controller('ContactoIndexController', ['DataService', ContactoIndexController]);
    function ContactoIndexController(DataService) {
        var vm = this;
        vm.label = {page: 'Pag.:', rowsPerPage: 'Mostrar:', of: 'de'};
        vm.params = {page: 1, limit: 50, order: 'nombre'};
        vm.contactos = {data: [], total: 0};

        vm.getData = function() {
            vm.promise = DataService.get('contactos', vm.params).then(function(response) {
                vm.contactos.data = response.data.data;
                vm.contactos.total = response.data.total;
            }).catch(function() {
                vm.contactos = {data: [], total: 0};
            });
        };

    }
})();