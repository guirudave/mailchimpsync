(function() {
    'use strict';
    angular.module('App').controller('LoginController', ['$rootScope', '$state', 'DataService', LoginController]);
    function LoginController($rootScope, $state, DataService) {
        var vm = this;

        vm.model = {};
        vm.login = function() {
            vm.form.$setSubmitted();
            if (vm.form.$valid) {
                vm.errors = [];
                vm.isLoading = true;
                DataService.post('login', vm.model)
                    .then(function(response) {
                        $rootScope.session = true;
                        return $state.transitionTo('home.dashboard');
                    })
                    .catch(function(response) {
                        $rootScope.session = false;
                        vm.errors = angular.fromJson(response.data).errors;
                    })
                    .finally(function() {
                        vm.isLoading = false;
                    });
            }
        };
    }
})();