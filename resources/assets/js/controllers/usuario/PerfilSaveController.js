(function() {
    'use strict';
    angular.module('App').controller('PerfilSaveController', ['DialogService', 'DataService', PerfilSaveController]);
    function PerfilSaveController(DialogService, DataService) {
        var vm = this;

        vm.model = {};
        vm.isLoading = true;
        DataService.get('perfil')
            .then(function(response) {
                vm.model = response.data;
            })
            .finally(function() {
                vm.isLoading = false;
            });

        vm.save = function() {
            vm.form.$setSubmitted();
            if (vm.form.$valid) {
                vm.errors = [];
                vm.isLoading = true;
                DataService.save('perfil', vm.model)
                    .then(function(data) {
                        DialogService.hide(data);
                    })
                    .catch(function(response) {
                        vm.errors = angular.fromJson(response.data).errors;
                    })
                    .finally(function() {
                        vm.isLoading = false;
                    });
            }
        };

        vm.cancel = DialogService.cancel();

    }
})();