(function() {
    'use strict';
    angular.module('App').controller('ListaSaveController', ['DialogService', 'DataService', 'model', ListaSaveController]);
    function ListaSaveController(DialogService, DataService, model) {
        var vm = this;
        vm.isEdit = model > 0;
        vm.model = model;

        vm.save = function() {
            vm.form.$setSubmitted();
            if (vm.form.$valid) {
                vm.errors = [];
                vm.isLoading = true;
                DataService.save('listas', vm.model)
                    .then(function(data) {
                        DialogService.hide(data);
                    })
                    .catch(function(response) {
                        vm.errors = angular.fromJson(response.data).errors;
                    })
                    .finally(function() {
                        vm.isLoading = false;
                    });
            }
        };

        vm.cancel = DialogService.cancel();

    }
})();