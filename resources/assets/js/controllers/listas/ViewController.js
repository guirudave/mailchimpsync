(function() {
    'use strict';
    angular.module('App').controller('ListaViewController', ['$stateParams', 'DataService', ListaViewController]);
    function ListaViewController($stateParams, DataService) {
        var vm = this;
        vm.id = $stateParams.id;

        vm.lista = {};
        vm.getData = function() {
            vm.isLoading = true;
            DataService.get('listas', {id: vm.id})
                .then(function(response) {
                    vm.lista = response.data;
                })
                .finally(function() {
                    vm.isLoading = false;
                });
        };

    }
})();