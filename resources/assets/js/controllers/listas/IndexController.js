(function() {
    'use strict';
    angular.module('App').controller('ListaIndexController', ['DialogService', 'DataService', ListaIndexController]);
    function ListaIndexController(DialogService, DataService) {
        var vm = this;
        vm.listas = [];

        vm.getData = function() {
            vm.promise = DataService.get('listas').then(function(response) {
                vm.listas = response.data;
            }).catch(function() {
                vm.listas = [];
            });
        };

        vm.create = function($event) {
            DialogService.show('listas.save', 'ListaSaveController', $event, {model: {permission_reminder: 'Estas suscrito a la '}}).then(function(response) {
                vm.listas.push(response.data);
            });
        };

        vm.edit = function($event, model) {
            var backup = angular.copy(model);
            DialogService.show('listas.save', 'ListaSaveController', $event, {model: model}).then(function(response) {
                model = response.data;
            }).catch(function() {
                angular.copy(backup, model);
            });
        };

        vm.delete = function($event, model) {
            DialogService.show('listas.delete', 'ListaDeleteController', $event, {model: model}).then(function() {
                vm.listas = _.without(vm.listas, model);
            });
        };

    }
})();