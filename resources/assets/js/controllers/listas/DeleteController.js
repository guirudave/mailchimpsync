(function() {
    'use strict';
    angular.module('App').controller('ListaDeleteController', ['DialogService', 'DataService', 'model', ListaDeleteController]);
    function ListaDeleteController(DialogService, DataService, model) {
        var vm = this;
        vm.model = model;

        vm.delete = function() {
            vm.errors = [];
            vm.isLoading = true;
            DataService.delete('listas', {id: vm.model.id, list: vm.model.id})
                .then(function(data) {
                    DialogService.hide(data);
                })
                .catch(function(response) {
                    vm.errors = angular.fromJson(response.data).errors;
                })
                .finally(function() {
                    vm.isLoading = false;
                });
        };

        vm.cancel = DialogService.cancel();

    }
})();