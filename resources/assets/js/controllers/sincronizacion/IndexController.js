(function() {
    'use strict';
    angular.module('App').controller('SyncIndexController', ['DataService', '$q', SyncIndexController]);
    function SyncIndexController(DataService, $q) {
        var vm = this;

        vm.cabeceras = [];
        vm.tiempos = [];
        vm.getData = function() {
            vm.isLoading = true;
            $q.all([
                DataService.get('sync/cabeceras'),
                DataService.get('sync/tiempos')
            ]).then(function(responses) {
                vm.cabeceras = responses[0].data;
                vm.tiempos = responses[1].data;
            }).finally(function() {
                vm.isLoading = false;
            });
        };

        vm.check = function(grupo, model) {
            vm.isLoading = true;
            DataService.put('sync/' + grupo, model)
                .then(function(response) {
                    model = response.data;
                })
                .catch(function() {
                    model.estatus = !model.estatus;
                })
                .finally(function() {
                    vm.isLoading = false;
                });
        };

        vm.sync = function() {
            vm.isLoading = true;
            vm.syncing = true;
            DataService.post('sync/lists').then(function() {
                DataService.post('sync/headers').then(function() {
                    DataService.post('sync/contacts').then(function() {
                        vm.isLoading = false;
                        vm.syncing = false;
                    })
                });
            });
        };
    }
})
();