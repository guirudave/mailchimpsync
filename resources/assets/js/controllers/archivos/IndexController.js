(function() {
    'use strict';
    angular.module('App').controller('ArchivoIndexController', ['Upload', 'DataService', '$q', ArchivoIndexController]);
    function ArchivoIndexController(Upload, DataService, $q) {
        var vm = this;
        vm.columnas = null;

        vm.archivos = [];
        vm.listas = [];
        vm.getData = function() {
            vm.isLoading = true;
            $q.all([
                DataService.get('archivos'),
                DataService.get('listas')
            ]).then(function(responses) {
                vm.archivos = responses[0].data;
                vm.listas = responses[1].data;
            }).finally(function() {
                vm.isLoading = false;
            });
        };

        vm.archivo = null;
        vm.lista = null;
        vm.contactos = [];
        vm.select = function() {
            if (vm.archivo && vm.lista) {
                vm.isLoading = true;
                DataService.get('archivos/' + vm.archivo, {lista: vm.lista})
                    .then(function(response) {
                        vm.contactos = response.data;
                    })
                    .finally(function() {
                        vm.isLoading = false;
                    });
            }
        };

        vm.upload = function() {
            if (vm.file) {
                vm.isLoading = true;
                Upload.upload({
                    url: 'archivos',
                    data: {file: vm.file}
                }).then(function(response) {
                    vm.archivos.push(response.data.data);
                }).finally(function() {
                    vm.isLoading = false;
                });
            }
        };

        vm.merge = function() {
            if (vm.archivo && vm.lista && vm.columnas) {
                vm.isLoading = true;
                DataService.post('sync/archivo', {archivo: vm.archivo, lista: vm.lista, cabeceras: vm.columnas}).finally(function() {
                    vm.isLoading = false;
                });
            }
        }
    }
})();