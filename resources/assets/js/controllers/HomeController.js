(function() {
    'use strict';
    angular.module('App').controller('HomeController', ['$mdSidenav', '$window', 'DialogService', HomeController]);
    function HomeController($mdSidenav, $window, DialogService) {
        var vm = this;

        vm.toggleLeftbar = function() {
            $mdSidenav('leftbar').toggle()
        };

        vm.logout = function() {
            $window.location.href = 'logout';
        };

        vm.profile = function($event) {
            DialogService.show('usuarios.perfil', 'PerfilSaveController', $event);
        };

    }
})();