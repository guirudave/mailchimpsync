(function() {
    'use strict';
    angular.module('App').service('DialogService', ['$mdDialog', DialogService]);
    function DialogService($mdDialog) {
        return {
            alert: function(title, content, btn) {
                return $mdDialog.show(
                    $mdDialog.alert()
                        .title(title)
                        .htmlContent(content)
                        .clickOutsideToClose(false)
                        .ariaLabel(btn)
                        .ok(btn)
                );
            },
            confirm: function(title, content, ok, cancel, event) {
                return $mdDialog.show(
                    $mdDialog.confirm()
                        .title(title)
                        .textContent(content)
                        .targetEvent(event)
                        .ariaLabel('Confirmation')
                        .ok(ok)
                        .cancel(cancel)
                );
            },
            show: function(tpl, controller, event, locals) {
                return $mdDialog.show({
                    templateUrl: 'views/' + tpl,
                    controller: controller,
                    controllerAs: 'vm',
                    clickOutsideToClose: true,
                    focusOnOpen: false,
                    targetEvent: event,
                    locals: locals
                })
            },
            hide: function(data) {
                return $mdDialog.hide(data);
            },
            cancel: function() {
                return $mdDialog.cancel;
            }
        };
    }
})();
