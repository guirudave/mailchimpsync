(function() {
    'use strict';
    angular.module('App').service('InterceptorService', ['$q', '$injector', '$window', '$rootScope', InterceptorService]);
    function InterceptorService($q, $injector) {
        this.responseError = function(response) {
            var DialogService = $injector.get('DialogService'),
                $state        = $injector.get('$state'),
                $rootScope    = $injector.get('$rootScope');
            if (response.status === 401) {
                $rootScope.session = false;
                DialogService.alert('La sesión ha expirado', "No estás autorizado para acceder a este recurdo. Debes iniciar sesión.", 'Iniciar sesión').then(function() {
                    $state.transitionTo('login');
                });
            } else if ([422, 404].indexOf(response.status) < 0) {
                DialogService.alert('Error', 'Error inesperado', 'Aceptar');
            }
            return $q.reject(response);
        };
    }
})();