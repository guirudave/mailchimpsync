(function() {
    'use strict';
    angular.module('App').service('DataService', ['$resource', DataService]);
    function DataService($resource) {
        var getResource = function(url) {
            return $resource(url + "/:id", {
                id: "@id"
            }, {
                get: {
                    method: "GET",
                    isArray: false
                },
                post: {
                    method: "POST",
                    isArray: false
                },
                put: {
                    method: "PUT",
                    isArray: false
                },
                delete: {
                    method: "DELETE",
                    isArray: false
                }
            });
        };
        return {
            get: function(url, params) {
                return getResource(url).get(params).$promise;
            },
            post: function(url, params) {
                return getResource(url).post(params).$promise;
            },
            put: function(url, params) {
                return getResource(url).put(params).$promise;
            },
            save: function(url, params) {
                return (params.id && (params.id > 0 || params.id !== '')) ? this.put(url, params) : this.post(url, params);
            },
            delete: function(url, params) {
                return getResource(url).delete(params).$promise;
            }
        };
    }
})();