(function() {
    'use strict';
    angular
        .module('App', ['ngMaterial', 'ngResource', 'ngSanitize', 'ui.router', 'md.data.table', 'ngFileUpload'])
        .config(['$urlRouterProvider', '$stateProvider', '$httpProvider', '$mdThemingProvider', config])
        .run(['$rootScope', '$state', '$window', run]);

    function config($urlRouterProvider, $stateProvider, $httpProvider, $mdThemingProvider) {
        /*--------------------------------------------------------------------------
        | Routes/States
        '-------------------------------------------------------------------------*/
        $urlRouterProvider.otherwise('/dashboard');
        $stateProvider
            .state('login', {
                url: '/login',
                templateUrl: 'views/login',
                controller: 'LoginController as vm',
                data: {
                    authentication: false
                }
            })

            .state('home', {
                url: '',
                abstract: true,
                templateUrl: 'views/home',
                controller: 'HomeController as vm',
                data: {
                    authentication: true
                }
            })
            .state('home.dashboard', {
                url: '/dashboard',
                templateUrl: 'views/dashboard.index'
            })
            .state('home.contactos', {
                url: '/contactos',
                templateUrl: 'views/contactos.index',
                controller: 'ContactoIndexController as vm'
            })
            .state('home.listas', {
                url: '/listas',
                templateUrl: 'views/listas.index',
                controller: 'ListaIndexController as vm'
            })
            .state('home.listas-view', {
                url: '/listas/:id',
                templateUrl: 'views/listas.view',
                controller: 'ListaViewController as vm'
            })

            .state('home.archivos', {
                url: '/archivos',
                templateUrl: 'views/archivos.index',
                controller: 'ArchivoIndexController as vm'
            })
            .state('home.sincronizacion', {
                url: '/sincronizacion',
                templateUrl: 'views/sincronizacion.index',
                controller: 'SyncIndexController as vm'
            });

        $httpProvider.interceptors.push('InterceptorService');

        /*--------------------------------------------------------------------------
        | Theme Config
        '-------------------------------------------------------------------------*/
        $mdThemingProvider.theme('default')
            .primaryPalette('deep-orange')
            .accentPalette('indigo')
            .warnPalette('orange');
    }

    function run($rootScope, $state, $window) {
        $rootScope.session = $window._session;
        $rootScope.$on('$stateChangeStart', function(event, toState) {
            var session        = $rootScope.session,
                authentication = toState.data.authentication;
            if (toState.name === 'login' && session) {
                event.preventDefault();
                $state.transitionTo('home.dashboard');
            } else if (toState.name !== 'login' && (authentication && !session)) {
                event.preventDefault();
                $state.transitionTo('login');
            }
        });
    }
}());